"""
Cloud pubsub logger
"""
import logging
import multiprocessing as mp
import time

# For Python 2 and Python 3 compatibility.
try:
    from queue import Empty
except ImportError:
    from Queue import Empty

# from server.modules.utils import check_topic
# from utils.utils import compat_urlsafe_b64encode
# from server.modules.utils import get_pubsub_client
from lib.utils import publish_body, RecoverableError

# from utils.pubsub_publisher import Publisher
from lib.pubsub_publisher import Publisher

BATCH_SIZE = 10
DEFAULT_POOL_SIZE = 1
DEFAULT_RETRY_COUNT = 10
DEFAULT_MAX_INTERVAL = 1.0


def send_loop(client, q, topic, logger):
    """Process loop for indefinitely sending logs to Cloud Pub/Sub.
    Args:
      client: Pub/Sub client. If it's None, a new client will be created.
      q: mp.JoinableQueue instance to get the message from.
      topic: Cloud Pub/Sub topic name to send the logs.
      retry: How many times to retry upon Cloud Pub/Sub API failure.
      logger: A logger for informing failures within this function.
      format_func: A callable for formatting the logs.
      publish_body: A callable for sending the logs.
    """

    while True:
        try:
            logs = q.get()
        except Empty:
            continue
        try:
            messages = [log for log in logs]
            client.publish_messages_in_batch(topic, messages)
            # nonbatch mode publishing. Slow!
            # for log in logs:
            #     print("Message: %s" %log)
            #     client.publish_message(topic, log)
        except RecoverableError as e:
            # Records the exception and puts the logs back to the deque
            # and prints the exception to stderr.
            q.put(logs)
            logger.exception(e)
        except Exception as e:
            logger.exception(e)
            logger.warn('There was a non recoverable error, exiting.')
            return
        q.task_done()


class AsyncPubsubHandler(logging.Handler):
    """A logging handler to publish logs to Cloud Pub/Sub in background."""
    def __init__(self, project, topic, worker_num=DEFAULT_POOL_SIZE,
                 client=None, stderr_logger=None,
                 max_interval=DEFAULT_MAX_INTERVAL,
                 batch_size=BATCH_SIZE):
        """The constructor of the handler.
        Args:
          topic: Cloud Pub/Sub topic name to send the logs.
          worker_num: The number of workers, defaults to 1.
          retry: How many times to retry upon Cloud Pub/Sub API failure,
                 defaults to 5.
          client: An optional Cloud Pub/Sub client to use. If not set, one is
                  built automatically, defaults to None.
          publish_body: A callable for publishing the Pub/Sub message,
                        just for testing and benchmarking purposes.
          stderr_logger: A logger for informing failures with this
                         logger, defaults to None and if not specified, a last
                         resort logger will be used.
          max_interval: An option to send messages after reaching some
                        time interval
        """
        super(AsyncPubsubHandler, self).__init__()
        self._q = mp.JoinableQueue()
        self._batch_size = batch_size
        self._buf = []
        self._max_interval = max_interval # max interval in seconds
        self._start_timestamp = time.time()
        self.project = project
        client = Publisher(project)

        if not stderr_logger:
            stderr_logger = logging.Logger('last_resort')
            stderr_logger.addHandler(logging.StreamHandler())

        for _ in range(worker_num):
            p = mp.Process(target=send_loop,
                           args=(client, self._q, topic, stderr_logger))
            p.daemon = True
            p.start()


    def emit(self, record):
        """Puts the record to the internal queue."""
        self._buf.append(self.format(record))
        now = time.time()

        if len(self._buf) == self._batch_size or now - self._start_timestamp > self._max_interval:
            self._q.put(self._buf)
            self._start_timestamp = time.time()
            self._buf = []


    def flush(self):
        """Blocks until the queue becomes empty."""
        with self.lock:
            if self._buf:
                self._q.put(self._buf)
                self._buf = []
            self._q.join()


    def close(self):
        """Joins the child processes and call the superclass's close."""
        with self.lock:
            self.flush()
        super(AsyncPubsubHandler, self).close()
