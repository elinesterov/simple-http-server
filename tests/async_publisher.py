# -*- coding: utf-8 -*-

import logging

from lib.async_handler import AsyncPubsubHandler
from lib import utils

def main():
    topic = 'test-logs'
    project = 'autodiscover-server'
    publish_body = utils.publish_body
    workers = 1
    pubsub_handler = AsyncPubsubHandler(project=project, topic=topic)
    # pubsub_handler = AsyncPubsubHandler(project, topic, workers, publish_body=publish_body)
    # pubsub_handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    json_formatter = logging.Formatter('{"timestamp": "%(created)f", '
                                    '"time": "%(asctime)s", "loglevel": "%(levelname)s", '
                                    '"request_data": %(message)s}')
    pubsub_handler.setFormatter(json_formatter)

    logger = logging.getLogger('root')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(pubsub_handler)

    # pubsub_handler.flush()
    for i in xrange(1):
        logger.info('My first message.')
        logger.info('My first message2.')
        logger.info('My first message3.')
        logger.info('My first message4. 😝')
    pubsub_handler.flush()


if __name__ == '__main__':
    main()
