# -*- coding: utf-8 -*-
import os
import time
import logging
from logging.handlers import TimedRotatingFileHandler
import multiprocessing as mp

from google.cloud import storage
from google.cloud.exceptions import GoogleCloudError

class GCSTimedRotatingFileHandler(TimedRotatingFileHandler):
    """
    Google cloud storage logging class
    In each rollover it deduplicated data and then upload it to a GCS
    """
    def __init__(self, bucket_name=None, stderr_logger=None, *args, **kwargs):
        super(GCSTimedRotatingFileHandler, self).__init__(*args, **kwargs)
        self.bucket_name = bucket_name
        if not stderr_logger:
            self.stderr_logger = logging.Logger('last_resort')
            self.stderr_logger.addHandler(logging.StreamHandler())

    def doRollover(self):
        """
        do a rollover; in this case, a date/time stamp is appended to the filename
        when the rollover happens.  However, you want the file to be named for the
        start of the interval, not the current time.  If there is a backup count,
        then we have to get a list of matching filenames, sort them and remove
        the one with the oldest suffix.
        """
        if self.stream:
            self.stream.close()
            self.stream = None
        # get the time that this sequence started at and make it a TimeTuple
        currentTime = int(time.time())
        dstNow = time.localtime(currentTime)[-1]
        t = self.rolloverAt - self.interval
        if self.utc:
            timeTuple = time.gmtime(t)
        else:
            timeTuple = time.localtime(t)
            dstThen = timeTuple[-1]
            if dstNow != dstThen:
                if dstNow:
                    addend = 3600
                else:
                    addend = -3600
                timeTuple = time.localtime(t + addend)
        dfn = self.baseFilename + "." + time.strftime(self.suffix, timeTuple)
        if os.path.exists(dfn):
            os.remove(dfn)

        os.rename(self.baseFilename, dfn)

        # Here we trigger GCP upload
        # we need a cicle trhough all files here because if some error
        # happened during GCP upload, we will be able to retry upload
        for s in self.getFilesToDelete():
            # Upload to GCP should be done in a queue or separate thread
            _, baseName = os.path.split(s)
            self.stderr_logger.info("Uploading: %s", baseName)
            src_file = s
            # TODO: not sure if we want files to be in different "folders"
            # will comment it out for now
            # prefix = baseName.split(".")[1].replace("-", "/").replace("_", "/")
            # blob_name = prefix + "/" + baseName
            blob_name = baseName
            # FIXME: currently we launch deduplocate and upload procedure
            # in a separate process. If chlid of the class set rollout interval less
            # then time needed for upload_blob method to finish, then
            # there will be race condition, so please make sure that you set
            # rollout interval much bigger then time needed for upload_blob
            # to happen.
            # Other fix would be fo use separate service which will be scaled
            # automatically depending on the file size.
            p = mp.Process(target=upload_blob,
                           args=(self.bucket_name, src_file, blob_name, self.stderr_logger))
            p.start()

        if not self.delay:
            self.stream = self._open()
        newRolloverAt = self.computeRollover(currentTime)
        while newRolloverAt <= currentTime:
            newRolloverAt = newRolloverAt + self.interval
        #If DST changes and midnight or weekly rollover, adjust for this.
        if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
            dstAtRollover = time.localtime(newRolloverAt)[-1]
            if dstNow != dstAtRollover:
                if not dstNow:  # DST kicks in before next rollover, so we need to deduct an hour
                    addend = -3600
                else:           # DST bows out before next rollover, so we need to add an hour
                    addend = 3600
                newRolloverAt += addend
        self.rolloverAt = newRolloverAt


def upload_blob(bucket_name, source_file_name, destination_blob_name,
                logger):
    """
    Uploads a file to the bucket.
    """
    # dedup source file first:
    dedup(source_file_name)

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    try:
        blob.upload_from_filename(source_file_name)
        logger.debug("File {} uploaded to {}.".format(
            source_file_name,
            destination_blob_name))
        logger.debug("Removing file {} .".format(source_file_name))
        os.remove(source_file_name)
    except (ValueError, GoogleCloudError) as e:
        logger.error("Error %s happened during file %s upload" %(e, source_file_name))


def dedup(source_file):
    """
    Deduplicate lines in a files
    TODO: implement better solution using intermediate bucket and standalone deduper
    """
    s = set()
    dedup_file = "/tmp/dedup.out"
    with open(dedup_file, "w") as out:
        for line in open(source_file):
            if line not in s:
                out.write(line)
                s.add(line)
    os.remove(source_file)
    os.rename(dedup_file, source_file)


def main():
    """
        Main method to test
    """
    log_filename = "/tmp/log_rotate"
    logger = logging.getLogger("MyLogger")
    logger.setLevel(logging.DEBUG)

    handler = GCSTimedRotatingFileHandler(bucket_name="xyu", filename=log_filename,
                                          when="s", interval=10)
    logger.addHandler(handler)
    for i in range(10000):
        time.sleep(0.1)
        # logger.debug('i=%d' % i)
        import random
        logger.debug("i=%d", random.randint(1, 10))

if __name__ == "__main__":
    main()
