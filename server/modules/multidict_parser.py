# -*- coding: utf-8 -*-
"""
werkzeug.datastructures.MultiDict to standart python dict converter
"""

class MultiDictParser(object):
    """
    werkzeug.datastructures.MultiDict to dict
    """

    def __init__(self, multidict):
        self.multidict = multidict

    def to_json(self):
        """
        converts MultiDict to regular json dict. It uses the samem method as
        MultiDict.to_dict(flat=Flase), but returns structure like
        {'name': 'v1', 'name': 'v2'} instead of {'name': ['v1', v2]}
        """
        res = {}
        for item in self.multidict.lists():
            if len(item[1]) > 1:
                val_list = []
                for value in item[1]:
                    val_list.append(value)
                res[item[0]] = val_list
            else:
                res[item[0]] = item[1][0]
        return res
