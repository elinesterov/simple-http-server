# -*- coding: utf-8 -*-
"""
Headers Parser module
"""
import json

from werkzeug.datastructures import Headers

from server.modules.exceptions import HeaderParserException

class HeadersParser(object):
    """Parse request headers
    """

    NGINX_ADDED_HEADERS = ['x-remote-addr',
                           'x-remote-port',
                           'x-server-addr',
                           'x-host',
                           'x-scheme',
                           'x-is-secure',
                           'x-server-protocol',
                           'x-server-port',
                           'x-tcp-rtt',
                           'x-tcp-rttvar',
                           'x-tcp-snd-cwd',
                           'x-tcp-rcv-space',
                          ]

    def __init__(self, headers_obj):
        """
        headers_obj: request.headers object
        """
        self.headers = Headers(headers_obj)

    def remove_extra_headers(self):
        """
        Remove extra headers such as Content-type and Content-length
        addede by ... TODO: I already do not remember what adds these headers
        """
        extra_headers = ['content-type',
                         'content-length'
                        ]
        for header in self.headers.items():
            h_name = header[0]
            h_value = header[1]
            if (h_name.lower() in extra_headers and h_value == '')\
                or (h_name.lower in extra_headers and h_value != ''):
                del self.headers[h_name]
        return True

    def extract_nginx_headers_data(self, remove_data=True):
        """method to extract data from nginx added headers
        """
        data = {}

        # if header name in NGINX_ADDED_HEADERS extract data

        for header in self.headers:
            h_name = header[0].lower()
            h_value = header[1]
            if h_name in self.NGINX_ADDED_HEADERS:
                data[h_name] = h_value

        if remove_data:
            self.remove_nginx_headers()

        # if header is missing set it's value to 0
        for header in self.NGINX_ADDED_HEADERS:
            if header not in data:
                data[header] = '0'
        return data

    def remove_nginx_headers(self):
        """ method to delete all Nginx added headers from request headers
        """
        for header in self.headers.items():
            h_name = header[0]
            if h_name.lower() in self.NGINX_ADDED_HEADERS:
                del self.headers[h_name]
        return True

    def headers_to_json(self):
        """
        Converts list of headers to list of header name: value pairs
        """
        # TODO: Instead of raising exception if one of headers cannot be parsed
        # it is better go through all header name value pairs and raise exception
        # for specific name or value
        res = {header[0].decode('utf8'): header[1].decode('utf8') for header in self.headers}
        # res = {header[0]: header[1] for header in self.headers}
        try:
            json.dumps(res, ensure_ascii=False)
        except Exception as err:
            raise HeaderParserException(err)
        return res
