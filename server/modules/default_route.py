# -*- coding: utf-8 -*-
"""
Wildcard request handler
"""

from flask import Blueprint

wildcard_request_handler = Blueprint("wildcard_request_handler", __name__)


@wildcard_request_handler.route('/', defaults={'path': ''},
                                methods=['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'OPTIONS'])
@wildcard_request_handler.route('/<path:path>',
                                methods=['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'OPTIONS'])
def handle_request(path):
    """
    Default response
    """
    return 'Ok\n'
